require 'test_helper'

class PageControllerControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get page_controller_index_url
    assert_response :success
  end

end
